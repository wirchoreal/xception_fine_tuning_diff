# KERAS XCEPTION FINE TUNING
# Fine tuning the Xception convolutional neural network (CNN),
# pre-trained on ImageNet, to create an arbitrary image
# classification model.

import os
import math
import pandas as pd
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.preprocessing.image import load_img, img_to_array
import pickle
from PIL import Image, ImageFile
import random
from os.path import join
from sklearn.metrics import f1_score
from sklearn.metrics import confusion_matrix
# from preprocessed_images_xception import transform_images


def transform_images(directory, val_names, val_labels, classes, s):
	val_x = np.array([img_to_array(load_img(os.path.join(directory, name), target_size = (299, 299))) for name in val_names])
	val_x = keras.applications.xception.preprocess_input(val_x)
	# Load val_y by converting val_labels to a float array
	val_y = np.array([[1.0 if clss == label else 0.0 for clss in classes] for label in val_labels])
	return val_x, val_y



def randomize(model, first_layer_index = 0):
	session = tf.keras.backend.get_session()
	for layer in model.layers[first_layer_index:]:
		if isinstance(layer, tf.keras.models.Model):
			randomize(layer)
			continue
		for value in layer.__dict__.values():
			if not hasattr(value, "initializer"): continue
			value.initializer.run(session = session)



def get_splitted_data(df, train_perc = 0.8):
	train_df = df.sample(frac = train_perc, replace=False, random_state= 42)
	train_names = set(train_df.index)
	df_names = set(df.index)
	val_names = df_names.difference(train_names)
	val_df = df.loc[val_names,:]
	return train_df, val_df 



def train_xception(directory, df_path, model_weights_path, s=299, bs=16, t=65, first_randomized_layer_index=115, load=0):
	df = pd.read_pickle(df_path)
	classes = list(df['labels'].unique())
	n = len(classes)
	train_df, val_df = get_splitted_data(df)
	train_names = list(train_df.index)
	train_labels =  list(train_df['labels'])
	val_names = list(val_df.index)
	val_labels =  list(val_df['labels'])
	with tf.Session() as sess:
		###
		### 1. Load training data (as a generator with random transforms):
		###
		# Tweak parameters below as needed
		generator = keras.preprocessing.image.ImageDataGenerator(
			rotation_range = 0,
			width_shift_range = 0,
			height_shift_range = 0,
			horizontal_flip = False,
			brightness_range = (0.3, 1.7),
			fill_mode = "nearest",	
			preprocessing_function = keras.applications.xception.preprocess_input
		)
		# Create generator
		batch_size = bs # Tweak if needed
		train_flow = generator.flow_from_dataframe(
			pd.DataFrame({"filename": train_names, "class": train_labels}, index = (range(len(train_names)))),
			x_col = "filename",
			y_col = "class",
			classes = classes,
			directory = directory,
			target_size = (s, s),
			batch_size = batch_size
		)
		print("[INFO] Loaded training data.")
		###
		### 2. Load validation data (as an array):
		###
		# Load val_x using Keras preprocessing functions
		val_x, val_y = transform_images(directory, val_names, val_labels, classes, s)
		print("[INFO] Loaded validation data.")
		###
		### 3. Load the Xception CNN, pre-trained on ImageNet, and
		###    trimmed so that the "top" (1000 labels) is not present:
		###
		# xception: (299, 299, 3) -> (10, 10, 2048)
		xception = keras.applications.xception.Xception(weights = "imagenet", include_top = False, input_shape = (s, s, 3))
		print("[INFO] Loaded xception CNN.")
		###
		### 4. Create the new "top" with n labels:
		###
		# top: (10, 10, 2048) -> (n,)
		top = keras.models.Sequential()
		top.add(keras.layers.GlobalAveragePooling2D(input_shape = (10, 10, 2048)))
		top.add(keras.layers.Dense(512))
		top.add(keras.layers.BatchNormalization())
		top.add(keras.layers.Activation("relu"))
		top.add(keras.layers.Dense(n))
		top.add(keras.layers.BatchNormalization())
		top.add(keras.layers.Activation("softmax"))
		print("[INFO] Created top layer.")
		###
		### 5. Create the model by combining xception and top:
		###
		# model: (299, 299, 3) -> (n,)
		model = keras.models.Sequential()
		# model.add(resnet)
		model.add(xception)
		model.add(top)
		print("[INFO] Combined xception and top layer.")
		###
		### 6. Freeze the first few layers (so they do not change during training)
		###
		num_frozen_layers = t # Tweak if needed
		for layer in xception.layers[:num_frozen_layers]: layer.trainable = False
		for layer in xception.layers[num_frozen_layers:]: layer.trainable = True
		print("[INFO] Some layers have been frozen.")
		if load == 1: 
			model.load_weights(model_weights_path)
			return
		###
		### 7. Randomize the last few layers' weights (reduces overfitting)
		###
		# randomize(xception, first_layer_index = first_randomized_layer_index)# Tweak if needed
		print("[INFO] Randomized last few layers.")
		###
		### 8. Compile model:
		###
		model.compile(
			loss = "categorical_crossentropy",
			optimizer = keras.optimizers.SGD(lr= 0.0, momentum= 0.5, decay= 0, nesterov= False),
			metrics = ["accuracy"]
		)
		print("[INFO] Compiled model.")
		###
		### 9. Train model:
		###
		print("[INFO] Training model.")
		learning_rates = [0.08 * 0.8 ** math.floor(i / 2) for i in range(20)] # Tweak if needed
		steps = math.ceil(len(train_names) / batch_size)
		scheduler = keras.callbacks.LearningRateScheduler(lambda i: learning_rates[i])
		checkpoint = keras.callbacks.ModelCheckpoint(model_weights_path, verbose= 1, save_best_only= True)
		model.fit_generator(
			train_flow,
			steps,
			validation_data= (val_x, val_y),
			epochs= len(learning_rates),
			callbacks= [scheduler, checkpoint],
			verbose= 2
		)
		print("[INFO] Saving model.")
		model.save(model_weights_path)

		print("[INFO] Getting confusion matrix and f1_score.")
		val_y_pred= model.predict(val_x)
		m = confusion_matrix(np.argmax(val_y,axis=1),np.argmax(val_y_pred,axis=1))
		print(m)
		f1_scores = f1_score(np.argmax(val_y,axis=1),np.argmax(val_y_pred,axis=1),average=None)
		print(f1_scores)
		return 


if __name__ == '__main__':
	import argparse
	ap = argparse.ArgumentParser()
	ap.add_argument( "--images", required = True, 
					help = "Path to folder with images")
	ap.add_argument("--sizes", nargs='+', type=int, default = [299,16],
		 			help="Train images with this image size and batch size")
	ap.add_argument("--version", default = '00',
		 			help="Training model version")
	ap.add_argument("--model", required=True, 
					help="Model weights path to be saved or load")
	ap.add_argument("--layers", type=int, default = 65,
					required=False, help="Number of cycles for the model")
	ap.add_argument("--index", type=int, default = 115,
					required=False, help="Number of cycles for the model")
	ap.add_argument("--load", type=int, default=0,
					help="whether or not to train the model")
	ap.add_argument("--dataframe-path", required=False,
					help="/path/to/dataframe with labels in .pkl")
	args = vars(ap.parse_args())
	model_path = args["model"] + args["version"] + ".hdf5"
	train_xception(args["images"], args['dataframe_path'], model_path, args["sizes"][0], args["sizes"][1], args["layers"], args["index"], args["load"])
